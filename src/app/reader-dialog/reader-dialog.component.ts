import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LibraryReader } from '../library.service';

@Component({
    selector: 'app-reader-dialog',
    templateUrl: './reader-dialog.component.html',
    styleUrls: ['./reader-dialog.component.css']
})
export class ReaderDialogComponent implements OnInit {

    edit?: boolean;
    constructor(
        public dialogRef: MatDialogRef<ReaderDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: LibraryReader,
    ) { }

    ngOnInit() {
        this.edit = false;
        console.log(this.data);
        if (!this.data?.name) {
            this.data = new LibraryReader();
        } else {
            this.edit = true;
        }
    }
    onNoClick(): void {
        this.dialogRef.close();
    }

}


export class DialogData {
    reader?: LibraryReader;
}
