import { Component, OnInit } from '@angular/core';
import { LibraryService, LibraryReader } from './library.service';
import { faEdit as faUserEdit, faTrashAlt as faTrash } from '@fortawesome/free-regular-svg-icons';
import { MatDialog } from '@angular/material/dialog';
import { ReaderDialogComponent } from './reader-dialog/reader-dialog.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title = 'library-front';
    readers: LibraryReader[] = [];
    faUserEdit = faUserEdit;
    faTrash = faTrash;

    constructor(public dialog: MatDialog, private libraryService: LibraryService) { }

    ngOnInit() {
        this.libraryService.getReaders().subscribe(r => this.readers = r);
    }

    delete(reader: LibraryReader) {
        this.libraryService.deleteReader(reader).subscribe(r => {
            let idx = this.readers.findIndex(r => r.id === reader.id);
            this.readers.splice(idx, 1);
        });
    }

    edit(reader?: LibraryReader) {
        const dialogRef = this.dialog.open(ReaderDialogComponent, {
            width: '450px',
            data: Object.assign({}, reader)
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                let idx = this.readers.findIndex(r => r.id === result.id)
                if (idx !== -1) {
                    this.libraryService.putReader(result).subscribe();
                    this.readers[idx] = result;
                } else {
                    this.libraryService.createReader(result).subscribe(r => {
                        this.readers.push(r);
                    }
                    );
                }
            }
        });
    }
}
